A C++ and OpenGL playground for testing different controls and shaders.
Might make it into a game/game engine one day.

TODO:

1. Finish the Material/Effect handler classes.
2. Create camera controller class, event handler class (plus mouse handling - clicking)
3. Think of some way to manage OpenGL buffers and rendering stuff
4. Implement object loader or create custom file format for loading 3D meshes (not so important, could use Assimp at the beginning)

![Alt text](./Screenshots/Screenshot_435.png?raw=true)
![Alt text](./Screenshots/Screenshot_436.png?raw=true)