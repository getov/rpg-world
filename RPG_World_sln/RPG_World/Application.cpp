#include "Application.h"
#include "OpenGLContext.h"
#include "GPUProgram.h"
#include "Camera.h"
#include "MaterialCreator.h"
#include <iostream>

void drawPlane(const Camera& camera, const glm::mat4& projection)
{
	glm::mat4 modelI = glm::mat4();
	glm::mat4 mv = camera.getViewMatrix() * modelI; /*glm::lookAt(tmpCamPos, glm::vec3(0, 0, 0), glm::vec3(0, 1, 0))*/
	glMatrixMode(GL_PROJECTION);
	glLoadMatrixf(&projection[0][0]);
	glMatrixMode(GL_MODELVIEW);
	glLoadMatrixf(&mv[0][0]);
	glScalef(15.f, 15.f, 15.f);
	glBegin(GL_QUADS);
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(-5.0f, -0.25f, -5.0f);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(5.0f, -0.25f, -5.0f);
	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(5.0f, -0.25f, 5.0f);
	glColor3f(1.0f, 0.0f, 1.0f);
	glVertex3f(-5.0f, -0.25f, 5.0f);

	glColor3f(1.0f, 0.0f, 1.0f);
	glVertex3f(-10.0f, -0.5f, -10.0f);
	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(10.0f, -0.5f, -10.0f);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(10.0f, -0.5f, 10.0f);
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(-10.0f, -0.5f, 10.0f);

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(-20.0f, -1.f, -20.0f);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(20.0f, -1.f, -20.0f);
	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(20.0f, -1.f, 20.0f);
	glColor3f(1.0f, 0.0f, 1.0f);
	glVertex3f(-20.0f, -1.f, 20.0f);

	glColor3f(1.0f, 0.0f, 1.0f);
	glVertex3f(-25.0f, -1.5f, -25.0f);
	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(25.0f, -1.5f, -25.0f);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(25.0f, -1.5f, 25.0f);
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(-25.0f, -1.5f, 25.0f);

	glEnd();
}

void drawGroundGrid(const Camera& camera, const glm::mat4& projection)
{
	glLineWidth(1.0f);

	glm::mat4 modelI = glm::mat4();
	glm::mat4 mv = camera.getViewMatrix() /** modelI*/; /*glm::lookAt(tmpCamPos, glm::vec3(0, 0, 0), glm::vec3(0, 1, 0))*/
	glMatrixMode(GL_PROJECTION);
	glLoadMatrixf(&projection[0][0]);
	glMatrixMode(GL_MODELVIEW);
	glLoadMatrixf(&mv[0][0]);
	glScalef(1.f, 1.f, 1.f);

	int lineLength = 400;
	float lineLengthHalf = 200;
	int step = 10;

	glBegin(GL_LINES);
	glColor3f(0.0f, 1.0f, 0.0f);
	
	for (int i = 0; i <= lineLength; i += step) 
	{
		glVertex3f((float)i - lineLengthHalf, 0.0f, -lineLengthHalf);
		glVertex3f((float)i - lineLengthHalf, 0.0f, lineLengthHalf);
		glVertex3f(-lineLengthHalf, 0.0f, (float)i - lineLengthHalf);
		glVertex3f(lineLengthHalf, 0.0f, (float)i - lineLengthHalf);
	}

	glEnd();
}

// This is test code for mouse scroll events
void scroll_callback(GLFWwindow* window, double xOffset, double yOffset)
{
	Camera* getCam = static_cast<Camera*>(glfwGetWindowUserPointer(window));
	if (yOffset<0.0)
	{
		getCam->position += getCam->getCameraUp() * MOVEMENT_SPEED*0.05f; // warcraft like zooming :D
	}
	if (yOffset>0.0)
	{
		getCam->position -= getCam->getCameraUp() * MOVEMENT_SPEED*0.05f;
		if (getCam->position.y <= 1e-2f)
		{
			getCam->position.y = 1e-2f;
		}
	}
}

Application& Application::GetInstance()
{
	static Application app;

	return app;
}

Application::Application()
{
}

Application::~Application()
{
}

int Application::Run()
{
	if (!OpenGLContext::Initialize(SCREEN_WIDTH, SCREEN_HEIGHT, "Game On!", NULL, NULL))
	{
		return EXIT_FAILURE;
	}

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);

	MaterialCreator::CreateMaterials();

	GPUProgram testProg;

	testProg.AddShader(GL_VERTEX_SHADER, "test.vert");
	testProg.AddShader(GL_FRAGMENT_SHADER, "test.frag");
	testProg.Link();
	testProg.Use();

	Camera testCam(glm::vec3(0, 30, 35), glm::vec3(0, 0, 0));
	//Camera testCam(vec3(1.0 / 3.0, 1.0 / 3.0, 1.0 / 3.0), glm::vec3(0, 0, 0));

	glm::mat4 projection = glm::perspective<float>(testCam.getFOV(), SCREEN_WIDTH / SCREEN_HEIGHT, testCam.getNear(), testCam.getFar());
	// 0											// 0
	/*glm::ortho<float>(0, SCREEN_WIDTH, SCREEN_HEIGHT, 0, testCam.getNear(), testCam.getFar())
	* glm::rotate(glm::mat4(), 35.264f, glm::vec3(1, 0, 0))
	* glm::rotate(glm::mat4(), -45.f, glm::vec3(0, 1, 0));*/

	glm::vec3 modelPos(0, 3, 0);
	mat4 translationMat = glm::translate(glm::mat4(), modelPos);
	glm::mat4 model = translationMat *
		glm::rotate(glm::mat4(), 45.f, glm::vec3(0, 1, 0)) *
		glm::scale(glm::mat4(), glm::vec3(1.f, 3.f, 1.f));

	glm::mat4 modelViewProjection = projection * testCam.getViewMatrix() * model;

	glm::vec3 lightPos = testCam.position;
	testProg.SetUniform("modelViewProjection", modelViewProjection);
	testProg.SetUniform("color", Color(0, 1, 0));
	testProg.SetUniform("model", model);
	testProg.SetUniform("cameraPosition", testCam.position);
	testProg.SetUniform("lightPosition", lightPos);

	glm::vec3 tmpCamPos = testCam.position; 

	GLuint vertexArrayID;
	glGenVertexArrays(1, &vertexArrayID);
	glBindVertexArray(vertexArrayID);

	GLuint vertexBuffer;
	glGenBuffers(1, &vertexBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertexBufferData), vertexBufferData, GL_STATIC_DRAW);

	// vertices
	glEnableVertexAttribArray(testProg.GetAttribLocation("vert"));
	glVertexAttribPointer(testProg.GetAttribLocation("vert"), 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), NULL);

	// connect the normal to the "vertNormal" attribute of the vertex shader
	glEnableVertexAttribArray(testProg.GetAttribLocation("normal"));
	glVertexAttribPointer(testProg.GetAttribLocation("normal"), 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), (const GLvoid*) (3 * sizeof(GLfloat)));

	//// connect the uv coords to the "vertTexCoord" attribute of the vertex shader
	//glEnableVertexAttribArray(testProg.getAttribLocation("	"));
	//glVertexAttribPointer(testProg.getAttribLocation("tex"), 2, GL_FLOAT, GL_TRUE, 8 * sizeof(GLfloat), (const GLvoid*) (3 * sizeof(GLfloat)));

	// unbind VBO and VAO
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
	testProg.StopUsing();

	// Turn on V-Sync at 60fps, for testing purposes only
	glfwSwapInterval(1);

	/* Loop until the user closes the window */
	double lastTime = glfwGetTime();
	while (!glfwWindowShouldClose(OpenGLContext::GetWindow()))
	{
		double currentTime = glfwGetTime();

		// this is called only when window resizing occurs
		glfwSetWindowSizeCallback(OpenGLContext::GetWindow(), OpenGLContext::ResizeViewport);

		translationMat = glm::translate(glm::mat4(), modelPos);
		model = translationMat *
			glm::rotate(glm::mat4(), 45.f, glm::vec3(0, 1, 0)) *
			glm::scale(glm::mat4(), glm::vec3(1.f, 3.f, 1.f));
		testCam.LookAt();
		glm::mat4 modelViewProjection = projection * testCam.getViewMatrix() * model;

		//glClearColor(0.55f, 0.8f, 0.95f, 0); //skyblue
		glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		testProg.Use();
		glBindVertexArray(vertexArrayID);
		testProg.SetUniform("color", Color(0, /*sin(currentTime - lastTime)*/1, 1));
		testProg.SetUniform("model", model);
		testProg.SetUniform("modelViewProjection", modelViewProjection);
		glDrawArrays(GL_TRIANGLES, 0, 36);
		glBindVertexArray(0);
		testProg.StopUsing();

		drawGroundGrid(testCam, projection);

		float elapsedTime = static_cast<float>(currentTime - lastTime);
		lastTime = currentTime;

		/* Swap front and back buffers */
		glfwSwapBuffers(OpenGLContext::GetWindow());

		/* Poll for and process events */
		if (glfwGetKey(OpenGLContext::GetWindow(), 'W') == GLFW_PRESS)
		{
			// move the camera and the character object
			testCam.moveCam(glm::vec3(0, 0, -1), MOVEMENT_SPEED*elapsedTime);
			modelPos += glm::vec3(0, 0, -1)*MOVEMENT_SPEED*elapsedTime;
		}
		if (glfwGetKey(OpenGLContext::GetWindow(), 'S') == GLFW_PRESS)
		{
			testCam.moveCam(glm::vec3(0, 0, 1), MOVEMENT_SPEED*elapsedTime);
			modelPos += glm::vec3(0, 0, 1)*MOVEMENT_SPEED*elapsedTime;
		}
		if (glfwGetKey(OpenGLContext::GetWindow(), 'A') == GLFW_PRESS)
		{
			testCam.moveCam(glm::vec3(-1, 0, 0), MOVEMENT_SPEED*elapsedTime);
			modelPos += glm::vec3(-1, 0, 0)*MOVEMENT_SPEED*elapsedTime;
		}
		if (glfwGetKey(OpenGLContext::GetWindow(), 'D') == GLFW_PRESS)
		{
			testCam.moveCam(glm::vec3(1, 0, 0), MOVEMENT_SPEED*elapsedTime);
			modelPos += glm::vec3(1, 0, 0)*MOVEMENT_SPEED*elapsedTime;
		}
		if (glfwGetKey(OpenGLContext::GetWindow(), 'O') == GLFW_PRESS)
		{
			testCam.position += testCam.getCameraUp() * MOVEMENT_SPEED*elapsedTime; // warcraft like zooming :D 
			//testCam.position += glm::vec3(0, 1, 1) * 0.01f; // zooom in-out, keep the same camera direction
			//testCam.moveCam(glm::vec3(0, 1, 0));
		}
		if (glfwGetKey(OpenGLContext::GetWindow(), 'P') == GLFW_PRESS)
		{
			testCam.position -= testCam.getCameraUp() * MOVEMENT_SPEED*elapsedTime;
			if (testCam.position.y <= 1e-2f)
			{
				testCam.position.y = 1e-2f;
			}
			//testCam.position += glm::vec3(0, -1, -1) * 0.01f;
			//testCam.moveCam(glm::vec3(0, -1, 0));
		}

		glfwSetWindowUserPointer(OpenGLContext::GetWindow(), &testCam);
		glfwSetScrollCallback(OpenGLContext::GetWindow(), scroll_callback);
		//glfwSetWindowUserPointer(OpenGLContext::GetWindow(), OpenGLContext::GetWindow());
		
		glfwPollEvents();
		//glfwSetWindowUserPointer(OpenGLContext::GetWindow(), NULL);
	}

	glfwTerminate();

	return EXIT_SUCCESS;
}