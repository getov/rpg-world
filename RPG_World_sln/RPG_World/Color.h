#ifndef COLOR_H
#define COLOR_H

class Color
{
public:

	union
	{
		struct { float r, g, b; };
		float rgb[3];
	};

	Color() 
	{
		SetToBlack();
	}

	Color(float red, float green, float blue)
	{
		SetColor(red, green, blue);
	}

	explicit Color(unsigned int rgb)
	{
		const float divider = 1.0f / 255.0f;
		b = (rgb & 0xff) * divider;
		g = ((rgb >> 8) & 0xff) * divider;
		r = ((rgb >> 16) & 0xff) * divider;
	}

	void SetColor(float red, float green, float blue)
	{
		r = red;
		g = green;
		b = blue;
	}

	void SetToBlack()
	{
		r = 0.f;
		g = 0.f;
		b = 0.f;
	}

	void operator += (const Color& rhs)
	{
		r += rhs.r;
		g += rhs.g;
		b += rhs.b;
	}
	
	void operator *= (float multiplier)
	{
		r *= multiplier;
		g *= multiplier;
		b *= multiplier;
	}
	
	void operator /= (float divider)
	{
		float rdivider = 1.0f / divider;
		r *= rdivider;
		g *= rdivider;
		b *= rdivider;
	}

	inline const float& operator[] (int index) const
	{
		return rgb[index];
	}

	inline float& operator[] (int index)
	{
		return rgb[index];
	}
};

inline Color operator + (const Color& a, const Color& b)
{
	return Color(a.r + b.r, a.g + b.g, a.b + b.b);
}

inline Color operator - (const Color& a, const Color& b)
{
	return Color(a.r - b.r, a.g - b.g, a.b - b.b);
}

inline Color operator * (const Color& a, const Color& b)
{
	return Color(a.r * b.r, a.g * b.g, a.b * b.b);
}

inline Color operator * (const Color& a, float multiplier)
{
	return Color(a.r * multiplier, a.g * multiplier, a.b * multiplier);
}

inline Color operator * (float multiplier, const Color& a)
{
	return Color(a.r * multiplier, a.g * multiplier, a.b * multiplier);
}

inline Color operator / (const Color& a, float divider)
{
	float mult = 1.0f / divider;
	return Color(a.r * mult, a.g * mult, a.b * mult);
}

#endif