#ifndef PRECOMPILED_H
#define PRECOMPILED_H

#include <GL\glew.h>
#include <GL\glfw3.h>
#include <glm\glm.hpp>
#include <glm\gtc\type_ptr.hpp>

#include <vector>
#include <fstream>
#include <sstream>
#include <iostream>

#include "Constants.h"

#endif