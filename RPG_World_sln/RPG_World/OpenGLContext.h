#ifndef OPENGL_CONTEXT
#define OPENGL_CONTEXT

#include "Precompiled.h"

class OpenGLContext
{
private:
	OpenGLContext(const OpenGLContext&) = delete;
	OpenGLContext& operator=(const OpenGLContext&) = delete;

public:
	/// Initialize GLFW, GLEW and create a GLFW window
	static int Initialize(int width, int height, const char* title, GLFWmonitor* monitor = nullptr, GLFWwindow* share = nullptr);

	/// Resize the OpenGL viewport when resizing/maximizing the GLFW window
	static void ResizeViewport(GLFWwindow* window, int height, int width);

	static GLFWwindow* GetWindow();
};

#endif