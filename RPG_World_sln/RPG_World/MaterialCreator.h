#ifndef MATERIAL_CREATOR
#define MATERIAL_CREATOR

class GPUProgram;
class IMaterial;

class MaterialCreator
{
private:
	static GPUProgram* phongEffect;
	static IMaterial* phongRGB;

	/// Create a shader program (called effect) from vertex, fragment and geometry (if given) shaders
	/// The function attaches and links the shader program
	/// If the operations are successful : return the created effect, else return nullptr
	static GPUProgram* CreateSingleShaderEffect(const char* vertexShader, const char* fragmentShader, const char* geometryShader = nullptr);

	/// @See - MaterialCreator::CreateSingleShaderEffect()
	/// Creates all the needed shader effects for the application
	/// Returns 1 if all the shader effects are created successfully, else returns 0
	static int CreateShaderEffects();

public:
	static int CreateMaterials();
};

#endif