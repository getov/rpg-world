#ifndef GPU_PROGRAM_H
#define GPU_PROGRAM_H

#include "Precompiled.h"
#include "Color.h"

class GPUProgram
{
private:
	GLuint glObject;
	std::vector<GLuint> shaders;

	GLint GetUniform(const char* name) const;

	bool IsInUse() const;

	GPUProgram(const GPUProgram& program) = delete;
	GPUProgram& operator=(const GPUProgram& program) = delete;

public:
	GPUProgram();
	~GPUProgram();

	int Link();

	void Use();
	void StopUsing();

	int AddShader(GLenum shaderType, const char* name);

	GLint GetAttribLocation(const char* name) const;
	void SetUniform(const GLchar* name, const glm::mat2& matrix, GLboolean transpose = GL_FALSE);
	void SetUniform(const GLchar* name, const glm::mat3& matrix, GLboolean transpose = GL_FALSE);
	void SetUniform(const GLchar* name, const glm::mat4& matrix, GLboolean transpose = GL_FALSE);
	void SetUniform(const GLchar* name, const glm::vec3& v);
	void SetUniform(const GLchar* name, const glm::vec4& v);
	void SetUniform(const GLchar* name, const Color& color);
};

#endif