#ifndef IMATERIAL_H
#define IMATERIAL_H

class GPUProgram;
class Color;

class IMaterial
{
public:
	virtual ~IMaterial();

	virtual void SetColor(const Color& color) = 0;
};

#endif