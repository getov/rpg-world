#ifndef CAMERA_H
#define CAMERA_H

#include <glm\glm.hpp>
#include <glm\gtc\matrix_transform.hpp>

using namespace glm;

class Camera
{
private:
	mat4 viewMatrix;
	vec3 cameraTarget;
	vec3 cameraUp;
	float fieldOfView;
	float nearPlane;
	float farPlane;

public:
	vec3 position;

	Camera();
	Camera(const vec3& position, const vec3& target);
	Camera(const vec3& position, const vec3& target,
		const float& fov, const float& near, const float& far);

	void lookAt(const vec3& target);

	const mat4& getViewMatrix() const;

	void moveCam(float dir);
	void moveCam(const glm::vec3& dir, float speed);
	void setCameraTarget(float dir);

	void LookAt();

	const vec3& getCameraUp() const;
	vec3 getForward();
	vec3 getBackward();
	vec3 getRightDir();
	vec3 getLeftDir();

	float getFOV();
	float getNear();
	float getFar();
};

#endif