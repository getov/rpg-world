#include "MaterialCreator.h"
#include "Precompiled.h"
#include "GPUProgram.h"
#include "IMaterial.h"
#include <memory>

GPUProgram* MaterialCreator::phongEffect;
IMaterial* MaterialCreator::phongRGB;

GPUProgram* MaterialCreator::CreateSingleShaderEffect(const char* vertexShader, const char* fragmentShader, const char* geometryShader)
{
	std::unique_ptr<GPUProgram> effect(new GPUProgram);

	if (!effect->AddShader(GL_VERTEX_SHADER, vertexShader))
	{
		return nullptr;
	}

	if (!effect->AddShader(GL_FRAGMENT_SHADER, fragmentShader))
	{
		return nullptr;
	}

	if (geometryShader)
	{
		if (!effect->AddShader(GL_GEOMETRY_SHADER, geometryShader))
		{
			return nullptr;
		}
	}

	if (!effect->Link())
	{
		return nullptr;
	}

	return effect.release();
}

int MaterialCreator::CreateShaderEffects()
{
	phongEffect = CreateSingleShaderEffect("test.vert", "test.frag");

	//std::cout << "phongEffect: " << (phongEffect && RESULT_OK) << std::endl;
	return (phongEffect && RESULT_OK); // have to check if it works
}

int MaterialCreator::CreateMaterials()
{
	int result = CreateShaderEffects();

	if (!result)
	{
		return RESULT_FAIL;
	}

	// phongRGB = new Material(phongEffect)
	return RESULT_OK;
}