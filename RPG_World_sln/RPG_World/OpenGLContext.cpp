#include "OpenGLContext.h"

GLFWwindow* window = nullptr;

int OpenGLContext::Initialize(int width, int height, const char* title, GLFWmonitor* monitor, GLFWwindow* share)
{
	if (!glfwInit())
	{
		std::cerr << "Failed to initialize GLFW!\n";
		return RESULT_FAIL;
	}

	window = glfwCreateWindow(width, height, title, monitor, share);

	if (!window)
	{
		glfwTerminate();
		return RESULT_FAIL;
	}

	/* Make the window's context current */
	glfwMakeContextCurrent(window);

	if (glewInit() != GLEW_OK)
	{
		std::cerr << "Failed to initialize GLEW!\n";
		return RESULT_FAIL;
	}

	return RESULT_OK;
}

void OpenGLContext::ResizeViewport(GLFWwindow* window, int width, int height)
{
	glfwGetWindowSize(window, &width, &height);
	glViewport(0, 0, width, height);
}

GLFWwindow* OpenGLContext::GetWindow()
{
	return window;
}