#include "GPUProgram.h"

#define GL_PROGRAM_NONE 0

GPUProgram::GPUProgram()
	: glObject(0)
	, shaders(0)
{
	glObject = glCreateProgram();
}

GPUProgram::~GPUProgram()
{
	glDeleteProgram(glObject);
}

int GPUProgram::AddShader(GLenum shaderType, const char* name)
{
	std::ifstream f;
	f.open(name, std::ios::in | std::ios::binary);
	std::stringstream buffer;
	buffer << f.rdbuf();
	f.close();

	GLuint shader = glCreateShader(shaderType);
	if (shader == GL_FALSE)
	{
		std::cerr << "glCreateShader failed\n";
		return 0;
	}

	std::string tmp(buffer.str());
	const char* source = tmp.c_str();
	glShaderSource(shader, 1, (const GLchar**) &(source), NULL);

	glCompileShader(shader);
	GLint status;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
	if (status == GL_FALSE)
	{
		std::cerr << "glCompileShader failed\n";
		glDeleteShader(shader);
		return status;
	}

	shaders.push_back(shader);
	return status;
}

int GPUProgram::Link()
{
	for (size_t i = 0; i < shaders.size(); ++i)
	{
		glAttachShader(glObject, shaders[i]);
	}

	glLinkProgram(glObject);

	// detach and delete the shaders
	for (size_t i = 0; i < shaders.size(); ++i)
	{
		glDetachShader(glObject, shaders[i]);
		glDeleteShader(shaders[i]);
	}

	GLint status;
	glGetProgramiv(glObject, GL_LINK_STATUS, &status);
	if (status == GL_FALSE) 
	{
		std::cerr << "glLinkProgram failed\n";
	}
	
	return status;
}

bool GPUProgram::IsInUse() const
{
	GLint currentProgram = 0;
	glGetIntegerv(GL_CURRENT_PROGRAM, &currentProgram);
	return (currentProgram == (GLint) glObject);
}

void GPUProgram::Use()
{
	glUseProgram(glObject);
}

void GPUProgram::StopUsing()
{
	assert(IsInUse());
	glUseProgram(GL_PROGRAM_NONE);
}

GLint GPUProgram::GetUniform(const char* name) const
{
	GLint status = -1;
	if (name)
	{
		status = glGetUniformLocation(glObject, name);
		if (status == -1)
		{
			std::cerr << "Program uniform not found: " << name << std::endl;
		}
	}
	else
	{
		std::cerr << "uniform is NULL\n";
	}

	return status;
}

GLint GPUProgram::GetAttribLocation(const char* name) const
{
	GLint status = -1;
	if (name)
	{
		status = glGetAttribLocation(glObject, name);
		if (status == -1)
		{
			std::cerr << "Program attribute not found: " << name << std::endl;
		}
	}
	else
	{
		std::cerr << "attribName is NULL\n";
	}

	return status;
}

void GPUProgram::SetUniform(const GLchar* name, const glm::mat2& matrix, GLboolean transpose)
{
	assert(IsInUse());
	glUniformMatrix2fv(GetUniform(name), 1, transpose, glm::value_ptr(matrix));
}

void GPUProgram::SetUniform(const GLchar* name, const glm::mat3& matrix, GLboolean transpose)
{
	assert(IsInUse());
	glUniformMatrix3fv(GetUniform(name), 1, transpose, glm::value_ptr(matrix));
}

void GPUProgram::SetUniform(const GLchar* name, const glm::mat4& matrix, GLboolean transpose)
{
	assert(IsInUse());
	glUniformMatrix4fv(GetUniform(name), 1, transpose, glm::value_ptr(matrix));
}

void GPUProgram::SetUniform(const GLchar* name, const glm::vec3& v)
{
	assert(IsInUse());
	glUniform3fv(GetUniform(name), 1, glm::value_ptr(v));
}

void GPUProgram::SetUniform(const GLchar* name, const glm::vec4& v)
{
	assert(IsInUse());
	glUniform4fv(GetUniform(name), 1, glm::value_ptr(v));
}

void GPUProgram::SetUniform(const GLchar* name, const Color& color)
{
	assert(IsInUse());
	glUniform3fv(GetUniform(name), 1, &(color.r));
}