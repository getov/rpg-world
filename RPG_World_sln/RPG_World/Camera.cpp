#include "Camera.h"

Camera::Camera() 
	: cameraTarget(vec3(0.0f, 0.0f, 0.0f))
	, cameraUp(vec3(1.0f, 0.0f, 0.0f))
	, fieldOfView(50.0f)
	, nearPlane(0.01f)
	, farPlane(10000.0f)
	, position(vec3(1.0f / 3.0f, 1.0f / 3.0f, 1.0f / 3.0f))
{
	vec3 look = glm::normalize(cameraTarget);
	vec3 right = glm::cross(look, vec3(0, 1, 0));
	vec3 up = glm::cross(look, glm::normalize(right));

	viewMatrix = glm::lookAt(position, cameraTarget, cameraUp);
}

Camera::Camera(const vec3& position, const vec3& target) 
	: cameraTarget(target)
	, cameraUp(vec3(0.0f, 1.0f, 0.0f))
	, fieldOfView(50.0f)
	, nearPlane(0.01f)
	, farPlane(10000.0f)
	, position(position)
{
	viewMatrix = glm::lookAt(this->position, cameraTarget, cameraUp);
}

Camera::Camera(const vec3& position, const vec3& target,
	const float& fov, const float& near, const float& far)
	: cameraTarget(target)
	, cameraUp(vec3(0, 1, 0))
	, fieldOfView(fov)
	, nearPlane(near)
	, farPlane(far)
	, position(position)
{
	viewMatrix = glm::lookAt(this->position, cameraTarget, cameraUp);
}

void Camera::lookAt(const vec3& target)
{
	viewMatrix = glm::lookAt(position, target, cameraUp);
}

const mat4& Camera::getViewMatrix() const
{
	return viewMatrix;
}

// Not sure why this is implemented like that.
// I was probably testing something
// TODO: Change it
void Camera::moveCam(float dir)
{
	glm::vec3 direction(1, 0, 0);
	position += direction * 0.01f * dir;
	cameraTarget += direction * 0.01f * dir;

	viewMatrix = glm::lookAt(position, cameraTarget, cameraUp);
}

void Camera::moveCam(const glm::vec3& dir, float speed)
{
	position += dir * speed;
	cameraTarget += dir * speed;
}

void Camera::LookAt()
{
	viewMatrix = glm::lookAt(position, cameraTarget, cameraUp);
}

// Not sure why this is implemented like that.
// I was probably testing something
// TODO: Change it
void Camera::setCameraTarget(float dir)
{
	cameraTarget += vec3(1, 0, 1) * 2.f * dir;

	viewMatrix = glm::lookAt(position, cameraTarget, cameraUp);
}

const vec3& Camera::getCameraUp() const
{
	return cameraUp;
}

vec3 Camera::getForward()
{
	return glm::normalize(cameraTarget - position);
}

vec3 Camera::getBackward()
{
	return -getForward();
}

vec3 Camera::getRightDir()
{
	return glm::cross(cameraUp, getForward());
}

vec3 Camera::getLeftDir()
{
	return -getRightDir();
}

float Camera::getFOV()
{
	return fieldOfView;
}

float Camera::getNear()
{
	return nearPlane;
}

float Camera::getFar()
{
	return farPlane;
}