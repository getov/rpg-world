#ifndef APPLICATION_H
#define APPLICATION_H

class Application
{
private:
	Application();
	~Application();

	void Destroy();

	Application(const Application&) = delete;
	Application& operator=(const Application&) = delete;
	
public:
	static Application& GetInstance();

	int Run();
};

#endif