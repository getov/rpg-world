#include "Precompiled.h"
#include "Application.h"

int main()
{
	Application& rpgWorld = Application::GetInstance();
	int result = rpgWorld.Run();

	return result;
}

