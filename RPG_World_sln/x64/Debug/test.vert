#version 150

uniform mat4 modelViewProjection;

in vec3 vert;
in vec3 normal;

out vec3 fragVert;
out vec3 fragNormal;

void main()
{
	fragVert = vert;
	fragNormal = normal;

	gl_Position = modelViewProjection * vec4(vert, 1);
}