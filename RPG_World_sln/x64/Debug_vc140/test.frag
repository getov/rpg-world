#version 150

uniform vec3 color;
uniform mat4 model;
uniform vec3 cameraPosition;
uniform vec3 lightPosition;

in vec3 fragVert;
in vec3 fragNormal;

out vec4 finalColor;

void main()
{
	vec3 lightColor = vec3(1, 1, 1);
	vec3 specularColor = vec3(1, 1, 1);

	vec3 normal = normalize(transpose(inverse(mat3(model))) * fragNormal);
	vec3 surfacePos = vec3(model * vec4(fragVert, 1));
	
    vec3 surfaceToLight = normalize(lightPosition - surfacePos);
    vec3 surfaceToCamera = normalize(cameraPosition - surfacePos);
    
    //ambient
    vec3 ambient = 0.2 * color * lightColor;

    //diffuse
    float diffuseCoefficient = max(0.0, dot(normal, surfaceToLight));
    vec3 diffuse = diffuseCoefficient * color * lightColor;
    
    //specular
    float specularCoefficient = 0.0;
    if(diffuseCoefficient > 0.0)
	{
        specularCoefficient = pow(max(0.0, dot(surfaceToCamera, reflect(-surfaceToLight, normal))), 80.0);
	}
    vec3 specular = specularCoefficient * specularColor * lightColor;
    
    //attenuation
    float distanceToLight = length(lightPosition - surfacePos);
    float attenuation = 1.0 / (1.0 + 0.00002 * pow(distanceToLight, 2)); // problem here

    //linear color (color before gamma correction)
    vec3 linearColor = ambient + attenuation*(diffuse + specular);

	//vec3 gamma = vec3(1.0/2.2);
    //finalColor = vec4(pow(linearColor, gamma), 1);

	finalColor = vec4(linearColor, 1);
}